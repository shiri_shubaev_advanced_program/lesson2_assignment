update Students set studentName="David", studentAge="17"
update Students set studentName="Mike", studentAge="17"
update Students set studentName="Anat", studentGrade="93"
insert into Students(studentId, studentName, studentAge) values(33945, "Jason", 20)
insert into Students(studentId, studentName, studentAge) values(25987, "George", 18)
insert into Students(studentId, studentName, studentAge) values(25985, "Mila", 19)
insert into Classes(classId, className, teacherID, catID) values(5569, "Architecture", 55625, 123)
insert into Teachers(teacherId, teacherName) values(55625, "Ted Mosby") 
insert into Teachers(teacherId, teacherName) values(1234, "Phillip")
insert into Categories(catId, catName) values(12, "General")
insert into Classes(classId, className, teacherID, catID) values(45698, "Education", 1234, 12)
insert into StudentsInClasses(studentID, classId) values(33945, 45698)
insert into StudentsInClasses(studentID, classId) values(25987, 45698)
insert into StudentsInClasses(studentID, classId) values(25985, 45698)
SELECT COUNT(*) FROM Teachers
SELECT AVG(studentGrade) FROM Students
